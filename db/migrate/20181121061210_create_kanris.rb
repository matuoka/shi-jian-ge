class CreateKanris < ActiveRecord::Migration
  def change
    create_table :kanris do |t|
      t.string :ka_id
      t.string :ka_pass

      t.timestamps null: false
    end
  end
end
