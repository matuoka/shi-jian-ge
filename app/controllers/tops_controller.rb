class TopsController < ApplicationController
  def main
	if session[:st_login_uid] then
	    @students = Student.all
        @subjects = Subject.all
	    render 'next'
	     else if session[:ka_login_uid] then
	        @kanris = Kanri.all
	        @subject = Subject.all
	        render 'next2'
	    else
	        render 'main'
        end
        end
  end

 def st_login
		student = Student.authenticate(params[:st_id], params[:st_pass])
		if student
			session[:st_login_uid] = params[:st_id]
			redirect_to root_path
		else
			render 'tops/st_login_form'
		end
 end
 
  def friend
      student = Student.find_by(st_id: params[:st_id])
      if student && student.st_name == params[:st_name]
			session[:st_login_uid] = params[:st_id]
			redirect_to items_path
		else
			flash[:alert] = 'IDもしくは名前に誤りがあります'
			render 'tops/friend_form'
      end
  end

 def ka_login
		kanri = Kanri.authenticate(params[:ka_id], params[:ka_pass])
		if kanri
			session[:ka_login_uid] = params[:ka_id]
			redirect_to tops_main_path
		else
			flash[:alert] = 'IDもしくはパスワードに誤りがあります'
			render 'tops/ka_login_form'
		end
 end

	def st_logout
		session.delete(:st_login_uid)
		redirect_to tops_st_login_path
	end
	
	def ka_logout 
		session.delete(:ka_login_uid)
		redirect_to tops_ka_login_path
	end
	
def edit
  @subject = Subject.find(params[:id])
  render edit_subject_path
end

def update 
  @subject =Subject.find(params[:id])
  @subject = Subject.update(
    kamoku: params[:subject][:kamoku], 
    professor: params[:subject][:professor],
    unit: params[:subject][:unit],
    youbi: params[:subject][:youbi],
    zigen: params[:subject][:zigen])
  redirect_to subjects_path
end 
	
 def destroy
  Subject.find(params[:id]).destroy
  render 'next2'
 end 
 
 def search
  if params[:st_id].present?
  @students = @students.get_by_st_id　params[:st_id]
  end
  if params[:st_name].present?
  @students = @students.get_by_st_name params[:st_name]
  end
  if params[:st_pass].present?
  @students = @students.get_by_st_pass params[:st_pass]
  end
 end
end