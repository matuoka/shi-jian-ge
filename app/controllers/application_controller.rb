class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  helper_method :current_schedule
  helper_method :current_user
  helper_method :current_user2
private
 def current_user
		if session[:st_login_uid]
			Student.find_by(st_id: session[:st_login_uid])
		end 
 end
 
 def current_user2
		if session[:ka_login_uid]
			Kanri.find_by(ka_id: session[:ka_login_uid])
		end 
 end

end



