class StudentsController < ApplicationController
  def index
    @students = Student.all
    
    if params[:st_id].present? 
    @students = @students.get_by_st_id params[:st_id]
    end  
    
    if params[:st_name].present? 
    @students = @students.get_by_st_name params[:st_name]
    end
  end

    
 def new
   @student = Student.new
 end
  
  def create
    @student = Student.new(
     st_id: params[:student][:st_id],
     st_name: params[:student][:st_name],
     password: params[:student][:password],
     password_confirmation: params[:student][:password_confirmation])
    if @student.save
      redirect_to tops_st_login_path
    else
      render 'new'
    end
  end

 def destroy
  Student.find(params[:id]).destroy
    redirect_to tops_st_login_path
 end 
 def search
    @students = Student.all
    
    if params[:st_id].present? 
    @students = @students.get_by_st_id params[:st_id]
    end  
    
    if params[:st_name].present? 
    @students = @students.get_by_st_name params[:st_name]
    end
    
 end
 def itiran
    @students = Student.all
 end
 end
