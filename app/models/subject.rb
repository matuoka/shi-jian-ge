class Subject < ActiveRecord::Base
 has_many :schedules
 has_many :items
 belongs_to :student
 belongs_to :kanri
end
