class Student < ActiveRecord::Base
   has_many :subjects
   has_many :schedules
  validates :st_id, presence: true, uniqueness: true
  validates :password, presence: true, confirmation: true
  attr_accessor :password
    def password=(val)
    if val.present?
      self.st_pass = BCrypt::Password.create(val)
    end
     @password = val
    end
    
   def self.authenticate(st_id, st_pass)
    student = Student.find_by_st_id(st_id)
    if student && BCrypt::Password.new(student.st_pass) == st_pass
      student
    else
      nil
    end
   end
      scope :get_by_st_id, ->(st_id) {
where("st_id like ?" ,"%#{st_id}%")
}
   scope :get_by_st_name, ->(st_name) {
where("st_name like ?" ,"%#{st_name}%")
}
end
